/**
 * Copyright (c) Peter Scargill a simple node-red node for Grovestreams.com
 */

module.exports = function(RED) {
	"use strict";
	var http = require('http');

	function groveNode(n) {
		RED.nodes.createNode(this, n);
		var node = this;
		node.api = n.api;
		node.component = n.component;
		node.stream = n.stream;
		node
			.on("input", function(inmsg) {
				if (node.stream>"") inmsg.payload=node.stream+"="+inmsg.payload;
				var options = {
				  host: 'grovestreams.com',
				  port: 80,
				  path: '/api/feed?api_key=' + node.api + '&compId=' + node.component + '&compName=' + node.component + '&' + inmsg.payload,
				  method: 'POST'
				};

				var req = http.request(options, function(res) {
				  console.log('STATUS: ' + res.statusCode);
				  console.log('HEADERS: ' + JSON.stringify(res.headers));
				  res.setEncoding('utf8');
				  res.on('data', function (chunk) {
					console.log('BODY: ' + chunk);
				  });
				});

				req.on('error', function(e) {
				  console.log('problem with request: ' + e.message);
				});

				// write data to request body
				req.end();						
			});
			node.on("close", function() {
		});
	}

	RED.httpAdmin.post("/grove/:id", RED.auth
			.needsPermission("grove.write"), function(req, res) {
		var node = RED.nodes.getNode(req.params.id);
		if (node != null) {
			try {
				node.emit("input", {
					payload : "reset"
				});
				res.send(200);
			} catch (err) {
				res.send(500);
				node.error("Inject failed:" + err);
			}
		} else {
			res.send(404);
		}
	});

	RED.nodes.registerType("grove", groveNode);
}
