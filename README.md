# Grove for Node-Red
A simple Grovestreams node for node-red

I created this to make talking to GroveStreams as simple as possible.

To install, go to your node-red directory and manually add a grove directory with your other nodes...
 that might be in usr/lib/node_modules or for pi users it might be in the pi-no
OR try this if you are familiar with NPM....possibly  sudo npm -g install git+https://bitbucket.org/scargill/grove.git

The only files you NEED are the .js, .html files and the icon directory with the image.

You can find out more about this at http://tech.scargill.net/grove/

This node has one input. In the setup you tell it your Grovestream PUT API key and the name of the component - in  the input you supply name and value pairs of one or more streams separated by ampersand. i.e.   fred=1  or fred=1&brian=2  OR you can specify the stream in the setup and merely send a value.